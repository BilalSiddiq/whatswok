<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingLandmarkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_landmark', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('listing_id');
            $table->unsignedInteger('landmark_id');
            $table->timestamps();

            $table->unique(['listing_id', 'landmark_id']);
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('landmark_id')->references('id')->on('landmarks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_landmark');
    }
}
