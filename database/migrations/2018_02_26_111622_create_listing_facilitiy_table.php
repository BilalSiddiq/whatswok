<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingFacilitiyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_facility', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('listing_id');
            $table->unsignedInteger('facility_id');
            $table->timestamps();

            $table->unique(['listing_id', 'facility_id']);
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('facility_id')->references('id')->on('facilities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_facility');
    }
}
