<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingSpecificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_specification', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('listing_id');
            $table->unsignedInteger('specification_id');
            $table->string('value', 255);
            $table->timestamps();

            $table->unique(['listing_id', 'specification_id']);
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('specification_id')->references('id')->on('specifications')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_specification');
    }
}
