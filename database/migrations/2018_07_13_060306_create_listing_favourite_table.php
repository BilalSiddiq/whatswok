<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingFavouriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_favourites', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('listing_id');
            $table->timestamps();

            $table->unique(['user_id', 'listing_id']);

            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::table('listings', function (Blueprint $table) {

            $table->unsignedInteger('favourite_count')->default(0)->after('is_negotiable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_favourites');

        Schema::table('listings', function (Blueprint $table) {

            $table->unsignedInteger('favourite_count')->default(0)->after('is_negotiable');
        });
    }
}
