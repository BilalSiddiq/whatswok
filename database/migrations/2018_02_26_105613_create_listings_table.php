<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('listing_type_id');
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('availability_id');
            $table->unsignedInteger('user_address_id');

            $table->string('title', 255);
            $table->float('price', 8, 2)->default(0.0);
            $table->tinyInteger('is_negotiable')->default(1);
            $table->string('description')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('listing_type_id')->references('id')->on('listing_types');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('availability_id')->references('id')->on('availabilities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
