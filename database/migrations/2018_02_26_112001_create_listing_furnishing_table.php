<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingFurnishingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_furnishing', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('listing_id');
            $table->unsignedInteger('furnishing_id');
            $table->timestamps();

            $table->unique(['listing_id', 'furnishing_id']);
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('furnishing_id')->references('id')->on('furnishings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_furnishing');
    }
}
