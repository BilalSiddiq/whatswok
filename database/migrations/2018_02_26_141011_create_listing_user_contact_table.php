<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingUserContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listing_user_contact', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('listing_id');
            $table->unsignedInteger('user_contact_id');
            $table->timestamps();

            $table->unique(['listing_id', 'user_contact_id']);
            $table->foreign('listing_id')->references('id')->on('listings')->onDelete('cascade');
            $table->foreign('user_contact_id')->references('id')->on('user_contacts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listing_user_contact');
    }
}
