<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableAddAddressContactInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('phone_number');
            $table->string('profile_picture', 255)->after('password')->nullable();
            $table->unsignedInteger('user_address_id')->nullable();
            $table->unsignedInteger('contact_id')->nullable();

            $table->foreign('user_address_id')->references('id')->on('user_addresses')->onDelete('set null');
            $table->foreign('contact_id')->references('id')->on('user_contacts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->dropForeign(['user_address_id']);
            $table->dropForeign(['contact_id']);

            $table->string('phone_number', 50)->after('username')->nullable();
            $table->dropColumn('profile_picture');
            $table->dropColumn('user_address_id');
            $table->dropColumn('contact_id');
        });
    }
}
