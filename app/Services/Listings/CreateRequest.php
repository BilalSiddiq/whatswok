<?php
/**
 * Created by PhpStorm.
 * User: adnansiddiq
 * Date: 05/03/2018
 * Time: 1:32 PM
 */

namespace App\Services\Listings;


use App\Models\Listings\Listing;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CreateRequest
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $info;

    /**
     * CreateRequest constructor.
     * @param User $user
     * @param array $info
     */
    public function __construct(User $user, array $info)
    {
        $this->user = $user;
        $this->info = $info;
    }

    /**
     * @param $objectKey
     * @param string $key
     * @return null | int
     */
    protected function getKeyOfObject($objectKey, $key = 'id')
    {
        if (isset($this->info[$objectKey]) && is_array($this->info[$objectKey])) {
            $object = $this->info[$objectKey];
            if (isset($object[$key])) {
                return $object[$key];
            }
        }
        return null;
    }

    /**
     * @param $key
     * @return array|null
     */
    protected function getArrayFromInfo($key)
    {
        if (isset($this->info[$key]) && is_array($this->info[$key])) {
            $object = $this->info[$key];
            return $object;
        }
        return null;
    }

    /**
     * @return Listing
     */
    public function call()
    {
        $listing = DB::transaction(function () {

            $info = $this->info;
            $user = $this->user;

            $listing = new Listing($info);

            $listing->user_id = $user->id;

            $listing->currency_id = $this->getKeyOfObject('currency');

            $listing->listing_type_id = $this->getKeyOfObject('listing_type');;

            $listing->availability_id = $this->getKeyOfObject('availability');

            if ($address_id = $this->getKeyOfObject('address')) {
                $listing->user_address_id = $address_id;
            } else if ($userAddress = $this->getArrayFromInfo('address')) {

                if (count($userAddress)) {
                    $address = $user->addresses()->create([
                        'address1' => isset($userAddress['address1']) ? $userAddress['address1']: null,
                        'address2' => isset($userAddress['address2']) ? $userAddress['address2']: null,
                        'area' => isset($userAddress['area']) ? $userAddress['area']: null,
                        'state' => isset($userAddress['state']) ? $userAddress['state']: null,
                        'city' => isset($userAddress['city']) ? $userAddress['city']: null,
                        'country' => isset($userAddress['country']) ? $userAddress['country']: null,
                        'lat' => isset($userAddress['lat']) ? $userAddress['lat']: null,
                        'lng' => isset($userAddress['lng']) ? $userAddress['lng']: null,
                        'postal_code' => isset($userAddress['postal_code']) ? $userAddress['postal_code']: null,
                    ]);

                    $listing->user_address_id = $address->id;
                }
            }

            $listing->save();

            if ($attachments = $this->getArrayFromInfo('media_attachments')) {

                $this->uploadResources($listing, $attachments);
            }

            if ($specifications = $this->getArrayFromInfo('specifications')) {
                foreach ($specifications as $specification) {
                    $listing->specifications()->attach($specification['id'], ['value' => $specification['value']]);
                }
            }

            if ($facilities =  $this->getArrayFromInfo('facilities')) {
                foreach ($facilities as $facility) {
                    $listing->facilities()->attach($facility['id']);
                }
            }

            if ($furnishings = $this->getArrayFromInfo('furnishings')) {
                foreach ($furnishings as $furnishing) {
                    $listing->furnishings()->attach($furnishing['id']);
                }
            }

            if ($landmarks = $this->getArrayFromInfo('landmarks')) {
                foreach ($landmarks as $landmark) {
                    $listing->landmarks()->attach($landmark['id']);
                }
            }

            if ($user_contacts = $this->getArrayFromInfo('user_contacts')) {
                foreach ($user_contacts as $user_contact) {
                    if (isset($user_contact['id'])) {
                        $listing->userContacts()->attach($user_contact['id']);
                    } else {
                        $contact = $user->contacts()->create([
                            'phone_number' => isset($user_contact['phone_number']) ? $user_contact['phone_number']: null,
                            'type' => isset($user_contact['address2']) ? $user_contact['address2']: null,
                        ]);
                        $listing->userContacts()->attach($contact->id);
                    }
                }
            }

            return $listing;

        });

        $listing->refresh();

        return $listing;
    }

    /**
     * @param Listing $listing
     * @param array $resources
     */
    protected function uploadResources(Listing $listing, array $resources)
    {
        foreach ($resources as $resource) {

            $src = $resource['src'];

            $tmp = $src->getRealPath();

            $content = file_get_contents($tmp);

            $ext = $src->getClientOriginalExtension();

            $time_stamp = Carbon::now();

            $modified_name = str_random(3) . md5($time_stamp) . '.' . $ext;

            Storage::put(Listing::ORIGINAL_IMAGE_PATH . $modified_name, $content, 'public');

            $image = Image::make($tmp);

            $stream = $image->fit(500, 500)->stream($ext, 100);
            Storage::put(Listing::MEDIUMS_IMAGE_PATH . $modified_name, $stream, 'public');

            $stream = $image->fit(100, 100)->stream($ext, 100);
            Storage::put(Listing::SMALL_IMAGE_PATH . $modified_name, $stream, 'public');

            $listing->mediaAttachments()->create([
                'src_small' => $modified_name,
                'src_medium' => $modified_name,
                'src' => $modified_name,
            ]);
        }
    }
}