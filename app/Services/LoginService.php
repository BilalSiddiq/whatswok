<?php
/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 1/14/18
 * Time: 9:26 PM
 */

namespace App\Services;


class LoginService
{
    private $username;
    private $password;

    /**
     * LoginService constructor.
     * @param $username
     * @param $password
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function login()
    {
        $credentials = [
            'email' =>$this->username,
            'passwoed'=>$this->password
        ];
        $token = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'response' => 'error',
                    'message' => 'invalid_email_or_password',
                ]);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'response' => 'error',
                'message' => 'failed_to_create_token',
            ]);
        }
        return response()->json([
            'response' => 'success',
            'result' => [
                'token' => $token,
            ],
        ]);
    }


}