<?php
namespace App\Services;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Created by PhpStorm.
 * User: bilal
 * Date: 1/14/18
 * Time: 2:46 PM
 */
class RegisterService
{
    /**
     * @var array
     */
    protected $info;

    /**
     * Registrations constructor.
     * @param array $info
     */
    public function __construct(array $info)
    {
        $this->info = $info;
    }


    public function register()
    {
        $user = new User($this->info);

        if (isset($this->info['password'])) {
            $user->password = Hash::make($this->info['password']);
        }

        $user->save();

        if (isset($this->info['contact'])) {
            $user->addContact($this->info['contact']);
        }

        if (isset($this->info['address'])) {
            $user->addAddress($this->info['address']);
        }
        return $user->refresh();
    }

}
