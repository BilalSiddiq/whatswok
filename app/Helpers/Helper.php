<?php
/**
 * Created by PhpStorm.
 * User: adnansiddiq
 * Date: 23/02/2018
 * Time: 1:43 PM
 */

namespace App\Helpers;


class Helper
{

    /**
     * @param $object
     * @param array $overwrite_values
     *
     * @return mixed
     */
    public static function ReplicateObject($object, $overwrite_values = [])
    {
        $clonedObject = $object->replicate();

        foreach ($overwrite_values as $key => $value) {
            $clonedObject->$key = $value;
        }

        $clonedObject->save();

        return $clonedObject;
    }

    /**
     * Get client key from possible input parameters.
     *
     * @param $request
     *
     * @return null|string
     */
    public static function GetClientKey($request)
    {
        if ($request->header('X-Client')) {
            return $request->header('X-Client');
        }
        return  null;
    }

    /**
     * @param $email
     * @return bool
     */
    public static function isEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param $phone
     * @return bool
     */
    public static function isPhone($phone)
    {
        return Helper::ValidateData(['phone' => $phone], ['phone' => 'phone']) == null;
    }

    /**
     * @param string $message
     *
     * @return array
     */
    public static function unauthorizedError($message = 'You are not authorized for this action.')
    {
        return [
            'code' => 401,
            'status' => 'fail',
            'message' => $message,
        ];
    }

    public static function randomCodeGenerate()
    {
        $characters = '0123456789';

        $random_string_length = 6;
        $string = '';
        for ($i = 0; $i < $random_string_length; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

}