<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MediaAttachment
 * @package App\Models
 *
 * @property int $id
 * @property int listing_id
 * @property string src_small
 * @property string src_medium
 * @property string src
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class MediaAttachment extends Model
{
    protected $table = 'media_attachments';

    protected $fillable = [
        'src_small',
        'src_medium',
        'src',
    ];

    protected $hidden = [
        'listing_id',
    ];

    /**
     * @param $value
     * @return string
     */
    public function getSrcSmallAttribute($value)
    {
        return route('listing.small', [
            'listing' => $this->listing_id,
            'filename' => $value,
        ]);
    }

    /**
     * @param $value
     * @return string
     */
    public function getSrcMediumAttribute($value)
    {
        return route('listing.medium', [
            'listing' => $this->listing_id,
            'filename' => $value,
        ]);
    }

    /**
     * @param $value
     * @return string
     */
    public function getSrcAttribute($value)
    {
        return route('listing.original', [
            'listing' => $this->listing_id,
            'filename' => $value,
        ]);
    }
}
