<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserAddress
 * @package App\Models
 *
 * @property int $id
 * @property string $address1
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $area
 * @property string $country
 * @property string $postal_code
 * @property float lat
 * @property float lng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 */
class UserAddress extends Model
{
    protected $table = 'user_addresses';

    protected $fillable = [
        'address1',
        'address2',
        'area',
        'city',
        'state',
        'country',
        'postal_code',
        'lat',
        'lng',
    ];

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float',
    ];

    protected $hidden = [
        'user_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
