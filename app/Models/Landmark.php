<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Landmark
 * @package App\Models
 *
 * @property int $id
 * @property string title
 * @property string description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Landmark extends Model
{
    protected $table = 'landmarks';

    protected $fillable = [
        'title',
        'description',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot',
    ];
}
