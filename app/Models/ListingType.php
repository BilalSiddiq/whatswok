<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingType
 * @package App\Models
 *
 * @property int $id
 * @property string title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ListingType extends Model
{
    protected $table = 'listing_types';

    protected $fillable = [
        'title',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'parent_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany | ListingType
     */
    public function subTypes()
    {
        return $this->hasMany(ListingType::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | ListingType
     */
    public function type()
    {
        return $this->belongsTo(ListingType::class, 'parent_id');
    }
}
