<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Favourite
 * @package App\Models\Listings
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 */
class Favourite extends Model
{
    protected $table = 'listing_favourites';

}
