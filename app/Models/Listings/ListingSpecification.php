<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingSpecification
 * @package App\Models\Listings
 *
 * @property int $id
 * @property int $listing_id
 * @property int $specification_id
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ListingSpecification extends Model
{
    protected $table = 'listing_specification';

    protected $fillable = [
        'value',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'specification_id',
        'listing_id',
    ];
}
