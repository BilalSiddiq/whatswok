<?php

namespace App\Models\Listings;

use App\Models\Availability;
use App\Models\Currency;
use App\Models\Facility;
use App\Models\Furnishing;
use App\Models\Landmark;
use App\Models\ListingType;
use App\Models\MediaAttachment;
use App\Models\Specification;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserContact;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Listing
 * @package App\Models\Listings
 *
 * @property int $id
 * @property int $user_id
 * @property int $listing_type_id
 * @property int $currency_id
 * @property int $availability_id
 * @property int $user_address_id
 *
 * @property string $title
 * @property float $price
 * @property bool $is_negotiable
 * @property string $description
 * @property float $lat
 * @property float $lng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $creator
 * @property ListingType $listingType
 * @property Currency $currency
 * @property Availability $availability
 *
 * @property Specification[] $specifications
 * @property Facility[] $facilities
 * @property Furnishing[] $furnishings
 * @property Landmark[] $landmarks
 * @property UserContact[] $userContacts
 * @property UserAddress $address
 * @property Favourite[] $favourites
 * @property int favourite_count
 */
class Listing extends Model
{
    protected $table = 'listings';

    const SMALL_IMAGE_PATH = 'app/listings/smalls/';
    const MEDIUMS_IMAGE_PATH = 'app/listings/mediums/';
    const ORIGINAL_IMAGE_PATH = 'app/listings/originals/';

    protected $fillable = [
        'title',
        'price',
        'is_negotiable',
        'description',
    ];

    protected $hidden = [
        'user_address_id',
        'user_id',
        'listing_type_id',
        'currency_id',
        'availability_id',
        'pivot',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | User
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | ListingType
     */
    public function listingType()
    {
        return $this->belongsTo(ListingType::class, 'listing_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | Currency
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | Availability
     */
    public function availability()
    {
        return $this->belongsTo(Availability::class, 'availability_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | UserAddress
     */
    public function address()
    {
        return $this->belongsTo(UserAddress::class, 'user_address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany | Specification
     */
    public function specifications()
    {
        return $this->belongsToMany(Specification::class, 'listing_specification', 'listing_id', 'specification_id')
            ->withTimestamps()->withPivot('value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany | Facility
     */
    public function facilities()
    {
        return $this->belongsToMany(Facility::class, 'listing_facility', 'listing_id', 'facility_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany | Furnishing
     */
    public function furnishings()
    {
        return $this->belongsToMany(Furnishing::class, 'listing_furnishing', 'listing_id', 'furnishing_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany | Landmark
     */
    public function landmarks()
    {
        return $this->belongsToMany(Landmark::class, 'listing_landmark', 'listing_id', 'landmark_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany | UserContact
     */
    public function userContacts()
    {
        return $this->belongsToMany(UserContact::class, 'listing_user_contact', 'listing_id', 'user_contact_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany | MediaAttachment
     */
    public function mediaAttachments()
    {
        return $this->hasMany(MediaAttachment::class, 'listing_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany | Listing
     */
    public function favourites()
    {
        return $this->hasMany(Favourite::class, 'listing_id');
    }
}
