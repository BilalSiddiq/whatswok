<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingUserContact
 * @package App\Models\Listings
 *
 * @property int $id
 * @property int $listing_id
 * @property int $user_contact_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ListingUserContact extends Model
{
    protected $table = 'listing_user_contact';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
