<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingFacility
 * @package App\Models\Listings
 *
 * @property int $id
 * @property int $listing_id
 * @property int $facility_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ListingFacility extends Model
{
    protected $table = 'listing_facility';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
