<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingLandmark
 * @package App\Models\Listings
 *
 * @property int $id
 * @property int $listing_id
 * @property int $landmark_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ListingLandmark extends Model
{
    protected $table = 'listing_landmark';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
