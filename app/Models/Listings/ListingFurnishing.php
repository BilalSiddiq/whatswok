<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingFurnishing
 * @package App\Models\Listings
 *
 * @property int $id
 * @property int $listing_id
 * @property int $furnishing_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ListingFurnishing extends Model
{
    protected $table = 'listing_furnishing';

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
