<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Availability
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Availability extends Model
{
    protected $table = 'availabilities';

    protected $fillable = [
        'title',
        'description',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
