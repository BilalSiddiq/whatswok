<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserContact
 * @package App\Models
 *
 * @property int $id
 * @property string $phone_number
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property User $user
 */
class UserContact extends Model
{
    protected $table = 'user_contacts';

    protected $fillable = [
        'phone_number',
        'type',
    ];

    protected $hidden = [
        'user_id',
        'pivot',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo | User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
