<?php

namespace App\Models;

use App\Models\Listings\Listing;
use Illuminate\Foundation\Auth\User as AuthenticatAble;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property int user_address_id
 * @property int contact_id
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property UserAddress address
 * @property UserAddress[] $addresses
 * @property UserContact[] $contacts
 * @property UserContact $contact
 */
class User extends AuthenticatAble
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'username',
        'gender',
        'dob',
        'about_me',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'created_by',
        'dob',
        'about_me',
        'username',
        'created_at',
        'updated_at',
        'user_address_id',
        'contact_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(UserAddress::class, 'user_address_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(UserContact::class, 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany | UserAddress
     */
    public function addresses()
    {
        return $this->hasMany(UserAddress::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany | UserContact
     */
    public function contacts()
    {
        return $this->hasMany(UserContact::class, 'user_id');
    }

    /**
     * @param array $addressInfo
     */
    public function addAddress($addressInfo)
    {
        if (!empty($addressInfo)) {
            $address = $this->address;
            if ($address) {
                $address->update($addressInfo);
            } else {
                $address = $this->addresses()->create($addressInfo);
                $this->user_address_id = $address->id;
            }
            $this->save();
        }
    }

    /**
     * @param $contactInfo
     */
    public function addContact($contactInfo)
    {
        if (isset($contactInfo['phone_number'])) {
            $contact = $this->contact;
            if ($contact) {
                $contact->update($contactInfo);
            } else {
                $contact = $this->contacts()->create($contactInfo);
                $this->contact_id = $contact->id;
            }
            $this->save();
        }
    }

    public function favouriteListings()
    {
        return $this->belongsToMany(Listing::class,
            'listing_favourites',
            'user_id',
            'listing_id')->withTimestamps();
    }
}
