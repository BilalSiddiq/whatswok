<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Currency extends Model
{
    protected $table = 'currencies';

    protected $fillable = [
        'title',
        'symbol',
        'description',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
