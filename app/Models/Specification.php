<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingSpecification
 * @package App\Models
 *
 * @property int $id
 * @property string name
 * @property string description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Specification extends Model
{
    protected $table = 'specifications';

    protected $fillable = [
        'title',
        'description',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot',
    ];

}
