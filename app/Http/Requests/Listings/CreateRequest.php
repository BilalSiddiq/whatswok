<?php

namespace App\Http\Requests\Listings;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'title' => 'required|max:150',
            'price' => 'required|integer|min:0',
            'is_negotiable' => 'required|in:0,1',
            'description' => 'required',
            'lat' => 'integer|min:-180|max:180',
            'lng' => 'integer|min:-180|max:180',

//            'media_attachments' => 'array',
//            'media_attachments.*.src' => 'image|max:2000',

            'currency' => 'required|array',
            'currency.id' => 'required|exists:currencies,id',

            'listing_type' => 'required|array',
            'listing_type.id' => 'required|exists:listing_types,id',

            'availability' => 'required|array',
            'availability.id' => 'required|exists:availabilities,id',

            'specifications' => 'array|min:1',
            'specifications.*.id' => 'required|exists:specifications,id',
            'specifications.*.value' => 'required|string',

            'facilities' => 'array|min:1',
            'facilities.*.id' => 'required|exists:facilities,id',

            'furnishings' => 'array|min:1',
            'furnishings.*.id' => 'required|exists:furnishings,id',

            'landmarks' => 'array|min:1',
            'landmarks.*.id' => 'required|exists:landmarks,id',

            'user_contacts' => 'array|min:1',
            'user_contacts.*.id' => 'exists:user_contacts,id',
            'user_contacts.*.phone_number' => 'string|max:20',
            'user_contacts.*.type' => 'string|max:20',

            'address' => 'array',
            'address.id' => 'exists:user_addresses,id',
            'address.postal_code' => 'integer',
        ];
    }

    public function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->after(function () use ($validator) {

            if ($validator->errors()->count() == 0) {

                if ($this->has('address')) {
                    $user_address = $this->get('address');
                    if (isset($user_address['id'])) {
                        if (!$this->user()->addresses()->where('user_addresses.id', $user_address['id'])->exists()) {
                            $validator->errors()->add('address', 'invalid user address');
                        }
                    }
                }

                if ($this->has('user_contacts')) {
                    $user_contacts = $this->get('user_contacts');
                    foreach ($user_contacts as $user_contact) {
                        if (isset($user_contact['id'])) {
                            if (!$this->user()->contacts()->where('user_contacts.id', $user_contact['id'])->exists()) {
                                $validator->errors()->add('user_contacts', 'invalid user user contacts');
                            }
                        } else if (!isset($user_contact['phone_number']) || empty($user_contact['phone_number'])) {
                            $validator->errors()->add('user_contacts', 'invalid user user contacts');
                        }
                    }
                }
            }

        });

        return $validator;
    }
}
