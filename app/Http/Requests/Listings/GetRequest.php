<?php

namespace App\Http\Requests\Listings;

use Illuminate\Foundation\Http\FormRequest;

class GetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string|min:1',
            'min_price' => 'integer|min:1',
            'max_price' => 'integer|min:1',
            'facilities' => 'string|min:1',
            'furnishings' => 'string|min:1',
            'landmarks' => 'string|min:1',
            'specifications' => 'string|min:1',
            'type' => 'integer|min:1',
            'availability' => 'integer|min:1'
        ];
    }
}
