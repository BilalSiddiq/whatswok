<?php

namespace App\Http\Requests;

/**
 * Class RequestRegister
 * @package App\Http\Requests
 *
 * @property string first_name
 * @property string last_name
 * @property string gender
 * @property string dob
 * @property string username
 * @property string email
 * @property string about_me
 * @property string password
 */

class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|unique:users|email|max:50',
            'username' => 'required|unique:users|max:50',
            'password' => 'required|min:6|max:50',
            'gender' => 'required|in:0,1',
            'dob' => 'required|date_format:Y-m-d',
            'about_me' => 'string|max:255',
            'contact' => 'array',
            'address' => 'array',
        ];
    }

}
