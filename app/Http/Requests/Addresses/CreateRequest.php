<?php

namespace App\Http\Requests\Addresses;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city' => 'string',
            'country' => 'string|required',
            'state' => 'string',
            'area' => 'string',
            'address1' => 'string',
            'address2' => 'string',
            'postal_code' => 'integer',
        ];
    }
}
