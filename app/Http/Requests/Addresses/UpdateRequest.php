<?php

namespace App\Http\Requests\Addresses;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $address = $this->route('address');

        return $this->user()->addresses()->where('user_addresses.id', $address->id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city' => 'string',
            'country' => 'string',
            'state' => 'string',
            'area' => 'string',
            'address1' => 'string',
            'address2' => 'string',
            'postal_code' => 'integer',
        ];
    }
}
