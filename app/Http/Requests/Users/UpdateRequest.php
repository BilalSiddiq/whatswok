<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class UpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string',
            'last_name' => 'string',
            'email' => [
                Rule::unique('users')->ignore($this->user()->id),
            ],
            'username' => [
                Rule::unique('users')->ignore($this->user()->id),
            ],
            'gender' => 'in:0,1',
            'dob' => 'date_format:Y-m-d',
            'about_me' => 'string|max:255',
            'contact' => 'array',
            'address' => 'array',
        ];
    }
}
