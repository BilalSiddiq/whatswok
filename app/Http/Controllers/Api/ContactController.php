<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Contacts;
use App\Http\Controllers\Controller;
use App\Models\UserContact;

class ContactController extends Controller
{
    /**
     * @param Contacts\GetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Contacts\GetRequest $request)
    {
        try {
            $contacts = $request->user()->contacts()->paginate(20);

            return $this->respondWithSuccess([
                'contacts' => $contacts,
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }


    /**
     * @param Contacts\CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Contacts\CreateRequest $request)
    {
        try {
            $address = $request->user()->contacts()->create($request->all());

            $address->refresh();

            return $this->respondWithSuccess([
                'contact' => $address,
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Contacts\UpdateRequest $request
     * @param UserContact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Contacts\UpdateRequest $request, UserContact $contact)
    {
        try {
            $contact->update($request->all());

            $contact->refresh();

            return $this->respondWithSuccess([
                'contact' => $contact,
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Contacts\DeleteRequest $request
     * @param UserContact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Contacts\DeleteRequest $request, UserContact $contact)
    {
        try {
            $contact->delete();

            return $this->respondWithSuccess([
                'message' => 'Contact deleted successfully',
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }
}
