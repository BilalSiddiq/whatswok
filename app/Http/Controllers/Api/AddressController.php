<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Addresses;
use App\Models\UserAddress;

class AddressController extends Controller
{
    /**
     * @param Addresses\GetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Addresses\GetRequest $request)
    {
        try {
            $address = $request->user()->addresses()->paginate(20);

            return $this->respondWithSuccess([
                'addresses' => $address,
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }


    /**
     * @param Addresses\CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Addresses\CreateRequest $request)
    {
        try {
            $address = $request->user()->addresses()->create($request->all());

            $address->refresh();

            return $this->respondWithSuccess([
                'address' => $address,
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Addresses\UpdateRequest $request
     * @param UserAddress $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Addresses\UpdateRequest $request, UserAddress $address)
    {
        try {
            $address->update($request->all());

            $address->refresh();

            return $this->respondWithSuccess([
                'address' => $address,
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Addresses\DeleteRequest $request
     * @param UserAddress $address
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Addresses\DeleteRequest $request, UserAddress $address)
    {
        try {
            $address->delete();


            return $this->respondWithSuccess([
                'message' => 'Address deleted successfully',
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }
}
