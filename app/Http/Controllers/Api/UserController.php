<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Users;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;

class UserController extends Controller
{
    /**
     * @param Users\UpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Users\UpdateRequest $request)
    {
        try {
            $user = $request->user();

            $user->update($request->all());

            if ($request->has('address')) {
                $addressInfo = $request->get('address');
                $user->addAddress($addressInfo);
            }

            if ($request->has('contact')) {
                $contactInfo = $request->get('contact');
                $user->addContact($contactInfo);
            }

            $user->refresh();

            return $this->respondWithSuccess([
                'user' => (new UserTransformer($user))->transform(),
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }
}
