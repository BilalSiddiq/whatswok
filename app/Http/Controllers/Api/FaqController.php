<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Faqs;
use App\Models\Faq;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    /**
     * @param Faqs\CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Faqs\CreateRequest $request)
    {
        try {
            $faq = Faq::create($request->all());

            return $this->respondWithSuccess([
                'faq' => $faq->fresh(),
            ]);

        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $faqs = Faq::paginate(20);

        return $this->respondWithSuccess([
            'faqs' => $faqs,
        ]);
    }


    /**
     * @param Faqs\UpdateRequest $request
     * @param Faq $faq
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Faqs\UpdateRequest $request, Faq $faq)
    {
        Faq::where('id', $faq->id)->update($request->all());

        return $this->respondWithSuccess([
            'faq' => $faq->fresh(),
        ]);
    }
}
