<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RegisterRequest;
use App\Transformers\LoginTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RegisterService;
use Tymon\JWTAuth\JWTAuth;

class RegisterController extends Controller
{

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function webRegister(RegisterRequest $request)
    {
        try
        {
            $userService = new RegisterService($request->all());

            $user = $userService->register();

            $token = \JWTAuth::fromUser($user);

            return $this->respondWithSuccess([
                'token' => $token,
                'user' => (new LoginTransformer($user))->transform(),
            ]);

        }catch (\Exception $exception){
            return $this->responseWithException($exception);
        }

    }
}
