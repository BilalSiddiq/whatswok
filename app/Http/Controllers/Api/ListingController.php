<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Listings;
use App\Models\Facility;
use App\Models\Furnishing;
use App\Models\Landmark;
use App\Models\Listings\Listing;
use App\Models\Specification;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Listings\CreateRequest;
use App\Transformers\ListingTransformer;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ListingController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPredefinedData()
    {
        return $this->respondWithSuccess([
            'facilities' => Facility::all(),
            'furnishings' => Furnishing::all(),
            'landmarks' => Landmark::all(),
            'specifications' => Specification::all(),
        ]);
    }

    /**
     * @param Listings\CreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Listings\CreateRequest $request)
    {
        try {
            $listing = (new CreateRequest($request->user(), $request->all()))->call();

            $listing->load(['creator',
                'listingType',
                'currency',
                'availability',
                'address',
                'specifications',
                'facilities',
                'furnishings',
                'landmarks',
                'userContacts',
                'mediaAttachments'
            ]);

            return $this->respondWithSuccess([
                'listing' => (new ListingTransformer($listing, $request->user()))->transform(),
            ]);

        } catch (\Exception $exception) {

            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Listings\GetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Listings\GetRequest $request)
    {
        $listings = Listing::with(['creator',
                'listingType',
                'currency',
                'availability',
                'address',
                'userContacts',
                'mediaAttachments',
            ]
        );

        if ($request->has('title')) {
            $listings->where('listings.title', 'like', "%{$request->get('title')}%");
        }

        if ($request->has('min_price')) {
            $listings->where('listings.price', '>=', $request->get('min_price'));
        }

        if ($request->has('max_price')) {
            $listings->where('listings.price', '<=', $request->get('max_price'));
        }

        if ($request->has('availability')) {
            $listings->where('listings.availability_id', $request->get('availability'));
        }

        if ($request->has('type')) {
            $listings->where('listings.listing_type_id', $request->get('type'));
        }

        if ($request->has('facilities')) {
            $query_string = $request->get('facilities');
            $query_ids = explode(',', $query_string);
            $listings->whereHas('facilities', function ($query) use ($query_ids) {
                return $query->whereIn('facilities.id', $query_ids);
            });
        }

        if ($request->has('furnishings')) {
            $query_string = $request->get('furnishings');
            $query_ids = explode(',', $query_string);
            $listings->whereHas('furnishings', function ($query) use ($query_ids) {
                return $query->whereIn('furnishings.id', $query_ids);
            });
        }

        if ($request->has('landmarks')) {
            $query_string = $request->get('landmarks');
            $query_ids = explode(',', $query_string);
            $listings->whereHas('landmarks', function ($query) use ($query_ids) {
                return $query->whereIn('landmarks.id', $query_ids);
            });
        }

        if ($request->has('specifications')) {
            $query_string = $request->get('specifications');
            $query_ids = explode(',', $query_string);
            $listings->whereHas('specifications', function ($query) use ($query_ids) {
                return $query->whereIn('specifications.id', $query_ids);
            });
        }

        $listings = $listings->paginate(20);

        $response = [];

        foreach ($listings as $listing) {
            $response [] = (new ListingTransformer($listing))->transform();
        }

        $listings->setCollection(collect($response));

        return $this->respondWithSuccess([
            'listings' => $listings,
        ]);
    }

    /**
     * @param Listing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Listing $listing)
    {
        $listing->load(['creator',
            'listingType',
            'currency',
            'availability',
            'address',
            'specifications',
            'facilities',
            'furnishings',
            'landmarks',
            'userContacts',
            'mediaAttachments'
        ]);

        return $this->respondWithSuccess([
            'listing' => (new ListingTransformer($listing))->transform(),
        ]);
    }

    /**
     * @param Listing $listing
     * @param $fileName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOriginal(Listing $listing, $fileName)
    {
        try {
            return Image::make(storage_path(Listing::ORIGINAL_IMAGE_PATH . $fileName))->response();
        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }

    }

    /**
     * @param Listing $listing
     * @param $fileName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSmall(Listing $listing, $fileName)
    {
        try {
            return Image::make(storage_path(Listing::SMALL_IMAGE_PATH . $fileName))->response();
        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Listing $listing
     * @param $fileName
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMedium(Listing $listing, $fileName)
    {
        try {
            return Image::make(storage_path(Listing::MEDIUMS_IMAGE_PATH . $fileName))->response();
        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Listings\FavouriteRequest $request
     * @param Listing $listing
     * @return \Illuminate\Http\JsonResponse
     */
    public function favourite(Listings\FavouriteRequest $request, Listing $listing)
    {
        try {

            $user = $request->user();

            if ($user->favouriteListings()->where('listings.id', $listing->id)->exists()) {
                $user->favouriteListings()->detach($listing->id);

                return $this->respondWithSuccess([
                    'message' => 'Listing remove from your favourite list.',
                ]);
            } else {
                $user->favouriteListings()->attach($listing->id);

                return $this->respondWithSuccess([
                    'message' => 'Listing add to your favourite.',
                ]);
            }


        } catch (\Exception $exception) {
            return $this->responseWithException($exception);
        }
    }

    /**
     * @param Listings\FavouriteRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFavourites(Listings\FavouriteRequest $request, User $user)
    {
        $listings = $user->favouriteListings()->with(['creator',
                'listingType',
                'currency',
                'availability',
                'address',
                'userContacts',
                'mediaAttachments',
            ]
        )->paginate(20);

        $response = [];

        foreach ($listings as $listing) {
            $response [] = (new ListingTransformer($listing))->transform();
        }

        $listings->setCollection(collect($response));

        return $this->respondWithSuccess([
            'listings' => $listings,
        ]);
    }
}
