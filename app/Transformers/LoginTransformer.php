<?php
/**
 * Created by PhpStorm.
 * User: adnansiddiq
 * Date: 28/02/2018
 * Time: 3:16 PM
 */

namespace App\Transformers;


use App\Models\User;

class LoginTransformer
{
    /**
     * @var User
     */
    protected $user;

    /**
     * LoginTransformer constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function transform()
    {

        $userResponse = (new UserTransformer($this->user))->transform();

        return $userResponse;
    }
}