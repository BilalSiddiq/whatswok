<?php
/**
 * Created by PhpStorm.
 * User: adnansiddiq
 * Date: 02/03/2018
 * Time: 7:56 AM
 */

namespace App\Transformers;


use App\Models\User;

class UserTransformer
{
    /**
     * @var User
     */
    protected $user;

    /**
     * UserTransformer constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function transform()
    {
        $this->user->load('address', 'contact');

        $this->user->makeVisible([
            'phone_number',
            'dob',
            'about_me',
            'username',
            'created_at',
            'updated_at',
        ]);

        return $this->user->toArray();
    }

}