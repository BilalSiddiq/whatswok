<?php
/**
 * Created by PhpStorm.
 * User: adnansiddiq
 * Date: 28/02/2018
 * Time: 12:56 PM
 */

namespace App\Transformers;


use App\Models\Listings\Listing;
use App\Models\User;

class ListingTransformer
{
    /**
     * @var Listing
     */
    protected $listing;

    /**
     * @var User
     */
    protected $user;

    /**
     * ListingTransformer constructor.
     * @param Listing $listing
     * @param User $user
     */
    public function __construct(Listing $listing, User $user = null)
    {
        $this->listing = $listing;
        $this->user = $user;
    }


    /**
     * @return array
     */
    public function transform()
    {
        if ($this->listing->relationLoaded('specifications')) {
            foreach ($this->listing->specifications as &$specification) {
                $specification->value = $specification->pivot->value;
            }
        }

        $this->listing->favourite_count = $this->listing->favourites()->count();

        return $this->listing->toArray();
    }

}