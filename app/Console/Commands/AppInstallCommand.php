<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AppInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flatmate:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save predefined data';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $commands = $this->getCommands();

        try{
            foreach ($commands as $command) {

                $this->call($command);

                $this->line('<info>Executed:</info> ' . str_replace(":", " ", $command));
            }
        }
        catch (\Exception $exception){
            throw $exception;
        }
    }

    /**
     * @return array
     */
    protected function getCommands()
    {
        return [
            'sync:facilities',
            'sync:listing-types',
            'sync:listing-specifications',
            'sync:furnishings',
            'sync:availabilities',
            'sync:landmarks',
            'sync:currencies',
        ];
    }
}
