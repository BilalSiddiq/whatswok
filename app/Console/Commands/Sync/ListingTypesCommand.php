<?php

namespace App\Console\Commands\Sync;

use App\Models\ListingType;
use Illuminate\Console\Command;

class ListingTypesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:listing-types';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Predefined Listing types and sub types';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $types = $this->getData();

        foreach ($types as $type => $subTypes) {

            $listingType = ListingType::firstOrCreate([
                'title' => $type
            ]);

            foreach ($subTypes as $subType) {
                $listingType->subTypes()->firstOrCreate([
                    'title' => $subType,
                ]);
            }
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            "Home" => [
                'Houses',
                'Flats',
                'Upper Portions',
                'Lower Portions',
                'Farm Houses',
                'Rooms',
                'Penthouse',
            ],
            "Plots" => [
                'Residential Plots',
                'Commercial Plots',
                'Agricultural Land',
                'Agricultural Land',
            ],
            'Commercial' => [
                'Offices',
                'Shops',
                'Warehouses',
            ]
        ];
    }
}
