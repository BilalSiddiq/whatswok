<?php

namespace App\Console\Commands\Sync;

use App\Models\Availability;
use Illuminate\Console\Command;

class AvailabilitiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:availabilities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync availabilities command';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = $this->getData();

        foreach ($data as $info) {

            $facility = Availability::firstOrNew([
                'title' => $info,
            ]);

            $facility->save();
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            'Freehold',
            'Available',
            '1 Month',
            '3 Months',
            '6 Months',
            '>6Months',
        ];
    }
}
