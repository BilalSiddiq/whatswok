<?php

namespace App\Console\Commands\Sync;

use App\Models\Furnishing;
use Illuminate\Console\Command;

class FurnishingsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:furnishings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync furnishings command';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = $this->getData();

        foreach ($data as $title) {

            Furnishing::firstOrCreate([
                'title' => $title,
            ]);
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            'Air-Conditioning',
            'Bed',
            'Dining Room Furniture',
            'Fridge',
            'High Floor',
            'Low Floor',
            'Living Room Furniture',
            'Oven / Microwave',
            'Television',
            'Washing Machine',
            'Water Heater',
            'Hair Dryer',
            'Hangers',
            'Shampoo',
        ];
    }
}
