<?php

namespace App\Console\Commands\Sync;

use App\Models\Facility;
use Illuminate\Console\Command;

class FacilitiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:facilities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Predefined Facilities';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = $this->getData();

        foreach ($data as $info) {

            $facility = Facility::firstOrNew([
                'title' => $info,
            ]);

            $facility->save();
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            'Cable TV',
            'Covered car park',
            'Gymnasium room',
            'Jacuzzi',
            'Launderette',
            'Multi-purpose hall',
            'Pavillion',
            'Reflexology Path',
            '24 hours security',
            'Squash court',
            'Swimming pool',
            'Tennis courts',
            'Wading pool',
            'Free Parking',
            'Wifi/ Internet',
            'Elevator',
            'Guest Access/ Lock box',
        ];
    }

}
