<?php

namespace App\Console\Commands\Sync;

use App\Models\Specification;
use Illuminate\Console\Command;

class ListingSpecificationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:listing-specifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Specification command';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $specificationData = $this->getData();

        foreach ($specificationData as $info) {

            $specification = Specification::firstOrNew([
                'title' => $info['name'],
            ]);

            $specification->description = $info['description'];
            $specification->save();
        }
    }

    protected function getData()
    {
        return [
            [
                'name' => 'Type',
                'description' => 'Rent/Sale/Lease',
            ],
            [
                'name' => 'FloorSize',
                'description' => 'Floor Size',
            ],
            [
                'name' => 'PSF',
                'description' => 'Price Per Square foot',
            ],
            [
                'name' => 'BuiltYear',
                'description' => 'Developed year',
            ],
            [
                'name' => 'HoldType',
                'description' => 'Freehold/ Tenant/ under development',
            ],
            [
                'name' => 'Developer',
                'description' => 'Name of developer',
            ],
            [
                'name' => 'Furnishing',
                'description' => 'Fully, Semi furnished,  unfurnished',
            ],
            [
                'name' => 'FloorLevel',
                'description' => 'Floor Level in number',
            ],
            [
                'name' => 'ListingDate',
                'description' => 'date of posting',
            ],
        ];
    }
}
