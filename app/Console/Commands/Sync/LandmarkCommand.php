<?php

namespace App\Console\Commands\Sync;

use App\Models\Landmark;
use Illuminate\Console\Command;

class LandmarkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:landmarks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = $this->getData();

        foreach ($data as $title) {

            Landmark::firstOrCreate([
                'title' => $title,
            ]);
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            'Hospitals and Health care',
            'Education',
            'Recreation and Public Places',
            'Restaurants and Hotels',
            'Retail and Wholesale',
            'Banks and Financial Institutions',
            'Travel, Tourism and Transportation',
            'Government Institutes',
            'Emergency & Rescue',
        ];
    }
}
