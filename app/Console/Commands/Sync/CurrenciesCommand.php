<?php

namespace App\Console\Commands\Sync;

use App\Models\Currency;
use Illuminate\Console\Command;

class CurrenciesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync currencies command';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = $this->getData();

        foreach ($data as $str) {

            $info = explode(',', $str);

            $facility = Currency::firstOrNew([
                'title' => $info[0],
                'symbol' => $info[1],
            ]);

            $facility->description = $info[2];

            $facility->save();
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            'Pakistan Rupees,PKR,Pakistani Rupees',
            'United States Dollar,USD,United States Dollar',
            'Malaysian Ringgit,MYR,Malaysian Ringgit',
        ];
    }
}
