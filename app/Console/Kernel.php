<?php

namespace App\Console;

use App\Console\Commands\AppInstallCommand;
use App\Console\Commands\Sync\AvailabilitiesCommand;
use App\Console\Commands\Sync\CurrenciesCommand;
use App\Console\Commands\Sync\FacilitiesCommand;
use App\Console\Commands\Sync\FurnishingsCommand;
use App\Console\Commands\Sync\LandmarkCommand;
use App\Console\Commands\Sync\ListingSpecificationsCommand;
use App\Console\Commands\Sync\ListingTypesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        AppInstallCommand::class,
        FacilitiesCommand::class,
        ListingTypesCommand::class,
        ListingSpecificationsCommand::class,
        FurnishingsCommand::class,
        AvailabilitiesCommand::class,
        LandmarkCommand::class,
        CurrenciesCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
