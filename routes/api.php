<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v1')->group(function () {

    Route::post('register', 'RegisterController@webRegister');

    Route::post('login', 'LoginController@webLogin');

    Route::post('faqs', 'FaqController@create');

    Route::get('faqs', 'FaqController@index');

    Route::put('faqs/{faq}', 'FaqController@update');

    Route::get('listing-predefined-data', 'ListingController@getPredefinedData');

    Route::get('listings', 'ListingController@index');

    Route::get('listings/{listing}', 'ListingController@show');

    Route::get('listings/{listing}/originals/{filename}', 'ListingController@getOriginal')->name('listing.original');
    Route::get('listings/{listing}/mediums/{filename}', 'ListingController@getMedium')->name('listing.medium');
    Route::get('listings/{listing}/smalls/{filename}', 'ListingController@getSmall')->name('listing.small');

    Route::get('users/{user}/favourite-listings', 'ListingController@getFavourites');

    Route::group(['middleware' => ['jwt.auth']], function () {

        Route::put('update-profile', 'UserController@update');

        Route::post('listings', 'ListingController@store');

        Route::post('listings/{listing}/favourite', 'ListingController@favourite');

        Route::get('addresses', 'AddressController@index');

        Route::post('addresses', 'AddressController@store');

        Route::put('addresses/{address}', 'AddressController@update');

        Route::delete('addresses/{address}', 'AddressController@delete');

        Route::get('contacts', 'ContactController@index');

        Route::post('contacts', 'ContactController@store');

        Route::put('contacts/{contact}', 'ContactController@update');

        Route::delete('contacts/{contact}', 'ContactController@delete');

    });
});
